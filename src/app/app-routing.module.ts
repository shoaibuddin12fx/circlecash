import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", redirectTo: "language", pathMatch: "full" },
  { path: "language", loadChildren: "./pages/language/language.module#LanguagePageModule" },
  { path: "tabs", loadChildren: "./pages/tabs/tabs.module#TabsPageModule" },
  { path: "profile", loadChildren: "./pages/profile/profile.module#ProfilePageModule" },
  { path: "login", loadChildren: "./pages/login/login.module#LoginPageModule" },
  { path: "registration", loadChildren: "./pages/registration/registration.module#RegistrationPageModule" },
  { path: "forgot", loadChildren: "./pages/forgot/forgot.module#ForgotPageModule" },
  // { path: "pay", loadChildren: "./pages/pay/pay.module#PayPageModule" },
  // { path: "preview", loadChildren: "./pages/preview/preview.module#PreviewPageModule" },
  // { path: "payment-history", loadChildren: "./pages/payment-history/payment-history.module#PaymentHistoryPageModule" },
  { path: "payment/:merchantID/:payAmount", loadChildren: "./pages/payment/payment.module#PaymentPageModule" },
  { path: "merchant/:merchantID", loadChildren: "./pages/merchant/merchant.module#MerchantPageModule" },
  { path: "thank", loadChildren: "./pages/thank/thank.module#ThankPageModule" },
  { path: "change-password", loadChildren: "./pages/change/change.module#ChangePageModule" },
  { path: 'create-password/:authToken/:userName', loadChildren: () => import('./pages/create-password/create-password.module').then(m => m.CreatePasswordPageModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
