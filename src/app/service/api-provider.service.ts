import { NavController, ToastController, LoadingController, AlertController, Events } from "@ionic/angular";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { HashLocationStrategy } from '@angular/common';
//import { User } from "./user.model";
export interface User {
  id: number;
  name: string;
  email: string;
  type: number;
  proPic: string;
}

@Injectable({
  providedIn: "root"
})
export class ApiProviderService {
  public header = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }
  public currentUser: User;
  public language: any = "en";
   //baseUrl = "https://www.circlecash.net/paymentApp/";
  baseUrl = "http://localhost:8888/paymentApp/";
  constructor(
    public http: HttpClient,
    public navCtrl: NavController,
    public loaderCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private events: Events
  ) {
    const data = JSON.parse(localStorage.getItem("circleUser"));
    if (data != undefined || data != null) {
      this.currentUser = {
        id: data.userId,
        name: data.name,
        email: data.email,
        type: data.role,
        proPic: data.profileImage
      };
    }
  }
  async  errAlert(header: string = "Alert", message: string = "Our server is in maintenance mode. plz! try after sometime.") {
    const alert = await this.alertCtrl.create({
      header,
      message,
      buttons: ["OK"]
    });
    return alert;
  }
  async  loginUser(data) {
    const loading = await this.loaderCtrl.create();
    loading.present();
    this.http.post<any>(this.baseUrl + "mLogin", JSON.stringify(data)).subscribe(res => {
      console.log({res});
      
      loading.dismiss();
      if (res["status"] == 1) {
        localStorage.setItem("circleUser", JSON.stringify(res["data"]));
        this.currentUser = {
          id: res["data"]["userId"],
          name: res["data"]["name"],
          email: res["data"]["email"],
          type: res["data"]["role"],
          proPic: res["data"]["profileImage"]
        };
        this.navCtrl.navigateRoot("/tabs");
        this.events.publish('user:proUpdate', JSON.stringify({ proImg: res["data"]["profileImage"] }));
      } else {
        this.toastCtrl.create({
          message: res['msg'],
          buttons: ['OK'],
          duration: 3000
        }).then(toast => toast.present());
      }
    }, async err => {
      console.log(err);
      loading.dismiss();
      const alert = await this.errAlert();
      alert.present();
    });
  }
  addUser(data) {
    return this.http.post(this.baseUrl + "mRegister", JSON.stringify(data));
  }
  getUserDetail(data) {
    return this.http.post(this.baseUrl + "mGetUserDetails", JSON.stringify(data));
  }
  updateUserDetails(data) {
    return this.http.post(this.baseUrl + "mUpdateUserDetails", JSON.stringify(data));
  }
  changeUserPasswordProfile(data) {
    return this.http.post(this.baseUrl + "mChangePassword", JSON.stringify(data));
  }
  getPaymentHistory(data) {
    return this.http.post(this.baseUrl + "mGetPaymentHistory", JSON.stringify(data));
  }
  dummyPayment(data) {
    return this.http.post(this.baseUrl + "dummyPayment", JSON.stringify(data));
  }
  forgotPasswordCustomer(data) {
    return this.http.post(this.baseUrl + "mCustomerForgotPassword", JSON.stringify(data));
  }
  setUserPassword(data) {
    return this.http.post(this.baseUrl + "mCreatePasswordCustomer", JSON.stringify(data));
  }
  changeUsrProfilePicture(data) {
    return this.http.post(this.baseUrl + "mChangeProfilePicture", JSON.stringify(data));
  }
  isLoggedIn() {
    return this.currentUser != null && this.currentUser != undefined;
  }
  logoutUser() {
    localStorage.clear();
    this.currentUser = null;
  }
}
