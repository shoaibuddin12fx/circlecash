import { NavController, MenuController, LoadingController, Events } from "@ionic/angular";
import { ApiProviderService } from "./../../service/api-provider.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-preview",
  templateUrl: "./preview.page.html",
  styleUrls: ["./preview.page.scss"]
})
export class PreviewPage implements OnInit {
  constructor(
    public apiProvider: ApiProviderService,
    private navCtrl: NavController,
    private menuCtrl: MenuController,
    private loaderCtrl: LoadingController,
    private events: Events
  ) {
    this.menuCtrl.enable(true);
    this.events.subscribe('user:proUpdate', res => {
      this.ionViewWillEnter();
    });
  }
  public profilePic: any;
  public dataUser = {
    userId: this.apiProvider.currentUser.id,
  };
  public userData: any;
  async ionViewWillEnter() {
    const loading = await this.loaderCtrl.create();
    loading.present();
    this.apiProvider.getUserDetail(this.dataUser).subscribe(res => {
      loading.dismiss();
      console.log(res);
      this.userData = {
        userName: res["data"]["username"],
        name: res["data"]["name"],
        email: res["data"]["email"],
        address: res["data"]["address"],
        country: res["data"]["country"],
        city: res["data"]["city"],
        zipCode: res["data"]["zipCode"],
        state: res["data"]["state"],
        mobile: res["data"]["mobile"]
      };
      if (res["data"]['profile_image'] != "") {
        this.profilePic = this.apiProvider.baseUrl + res["data"]['profile_image'];
      } else {
        this.profilePic = './assets/imgs/menu-profile.png';
      }
      console.log(this.profilePic);
    }, async err => {
      loading.dismiss();
      const alert = await this.apiProvider.errAlert();
      alert.present();
    });
  }
  //---------------------------------------
  myImage() {
    this.profilePic = "./assets/imgs/menu-profile.png";
  }
  //---------------------------------------
  previewProfile() {
    this.navCtrl.navigateForward("/profile");
  }
  ngOnInit() { }
}
