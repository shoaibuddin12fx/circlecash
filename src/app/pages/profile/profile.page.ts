import { ApiProviderService } from "./../../service/api-provider.service";
import { Component, OnInit } from "@angular/core";
import {
  NavController,
  ToastController,
  LoadingController,
  AlertController,
  ActionSheetController,
  Events
} from "@ionic/angular";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { Platform } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: "app-profile",
  templateUrl: "./profile.page.html",
  styleUrls: ["./profile.page.scss"]
})
export class ProfilePage implements OnInit {
  profilePic: any;
  userProfile = "personalInfo";

  constructor(
    public apiProvider: ApiProviderService,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private camera: Camera,
    private loaderCtrl: LoadingController,
    private actionSheetCtrl: ActionSheetController,
    public formBuilder: FormBuilder,
    private translate: TranslateService,
    private events: Events
  ) { }
  public isPassNotMatch = false;
  public base64Image: any = {
    userId: this.apiProvider.currentUser.id,
    profileImage: "",
    profileImageName: Math.floor(Math.random() * 10000000000000) + 1 + this.apiProvider.currentUser.id + ".jpeg"
  };
  public dataUser = { userId: this.apiProvider.currentUser.id };
  public userData: any = {
    userName: "",
    email: "",
    name: "",
    address: "",
    country: "",
    city: "",
    zipCode: "",
    state: "",
    mobile: "",
    userId: this.apiProvider.currentUser.id
  };
  public userPasswordData: any = {
    oldPassword: "",
    newPassword: "",
    userId: this.apiProvider.currentUser.id
  };
  public isSubmited: boolean = false;
  public isSubmited2: boolean = false;
  public editProfileForm: FormGroup;
  public changePassForm: FormGroup;
  ngOnInit() {
    const EMAILPATTERN = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-z\-0-9]+\.)+[a-z]{2,}))$/;
    this.editProfileForm = this.formBuilder.group({
      name: new FormControl('', Validators.compose([Validators.required])),
      uName: new FormControl('', Validators.compose([Validators.required])),
      mobile: new FormControl('', Validators.compose([Validators.required])),
      email: new FormControl('', Validators.compose([Validators.pattern(EMAILPATTERN), Validators.required])),
      address: new FormControl('', Validators.compose([Validators.required])),
      country: new FormControl('', Validators.compose([Validators.required])),
      state: new FormControl('', Validators.compose([Validators.required])),
      city: new FormControl('', Validators.compose([Validators.required])),
      zipCode: new FormControl('', Validators.compose([Validators.required])),
    });
    this.changePassForm = this.formBuilder.group({
      oldPass: new FormControl('', Validators.compose([Validators.required])),
      newPass: new FormControl('', Validators.compose([Validators.required]))
    });
  }
  public validation_messages = {
    'name': [
      { type: 'required', message: 'vMsg.nameReq' }
    ],
    'uName': [
      { type: 'required', message: 'vMsg.uNameReq' },
    ],
    'email': [
      { type: 'required', message: 'vMsg.emailReq' },
      { type: 'pattern', message: 'vMsg.validEmail' }
    ],
    'mobile': [
      { type: 'required', message: 'vMsg.mobileReq' }
    ],
    'address': [
      { type: 'required', message: 'vMsg.addReq' }
    ],
    'country': [
      { type: 'required', message: 'vMsg.countryReq' }
    ],
    'state': [
      { type: 'required', message: 'vMsg.stateReq' }
    ],
    'city': [
      { type: 'required', message: 'vMsg.cityReq' }
    ],
    'zipCode': [
      { type: 'required', message: 'vMsg.zipReq' }
    ],
    'oldPass': [
      { type: 'required', message: 'vMsg.oldPassReq' }
    ],
    'newPass': [
      { type: 'required', message: 'vMsg.newPassReq' }
    ]
  };
  ionViewWillEnter() {
    if (!this.apiProvider.isLoggedIn()) {
      this.navCtrl.navigateForward("/login");
    } else {
      this.apiProvider.getUserDetail(this.dataUser).subscribe(res => {
        console.log(res);
        this.userData = {
          userName: res["data"]["username"],
          name: res["data"]["name"],
          email: res["data"]["email"],
          address: res["data"]["address"],
          country: res["data"]["country"],
          city: res["data"]["city"],
          zipCode: res["data"]["zipCode"],
          state: res["data"]["state"],
          mobile: res["data"]["mobile"],
          userId: this.apiProvider.currentUser.id
        };
        if (res["data"]['profile_image'] != "") {
          this.profilePic = this.apiProvider.baseUrl + res["data"]['profile_image'];
        } else {
          this.profilePic = './assets/imgs/menu-profile.png';
        }
        console.log(this.profilePic);
      }, async err => {
        const alert = await this.apiProvider.errAlert();
        alert.present();
      });
    }
  }
  async updateUserProfile() {
    if (this.editProfileForm.valid) {
      const loading = await this.loaderCtrl.create();
      loading.present();
      let successMsg: any
      this.translate.get("resMsg").subscribe((res: any) => {
        successMsg = res;
      });
      this.apiProvider.updateUserDetails(this.userData).subscribe(
        async res => {
          loading.dismiss();
          if (res['status'] == 1) {
            const toastLogin = await this.toastCtrl.create({
              message: successMsg.proUpdated,
              duration: 3000,
              buttons: [successMsg.ok]
            });
            toastLogin.present();
            toastLogin.onWillDismiss().then(res => this.navCtrl.navigateRoot("/tabs"));
          } else {
            const toastLogin = await this.toastCtrl.create({
              message: successMsg.proFail,
              duration: 3000,
              buttons: [successMsg.ok]
            });
            toastLogin.present();
          }
        },
        async err => {
          loading.dismiss();
          const alert = await this.apiProvider.errAlert();
          alert.present();
        }
      );
    } else {
      this.isSubmited = true;
    }
  }

  async changeUserPasswordProfile() {
    if (this.changePassForm.valid) {
      const loading = await this.loaderCtrl.create();
      loading.present();
      let successMsg: any
      this.translate.get("resMsg").subscribe((res: any) => {
        successMsg = res;
      });
      this.apiProvider.changeUserPasswordProfile(this.userPasswordData).subscribe(
        async res => {
          loading.dismiss();
          if (res['status'] == 1) {
            const toastLogin = await this.toastCtrl.create({
              message: successMsg.passChanged,
              duration: 3000,
              buttons: [successMsg.ok]
            });
            toastLogin.present();
            toastLogin.onWillDismiss().then(res => this.navCtrl.navigateRoot("/tabs"));
          } else {
            const toastLogin = await this.toastCtrl.create({
              message: res["msg"],
              duration: 3000,
              buttons: [successMsg.ok]
            });
            toastLogin.present();
          }
        },
        async err => {
          const alert = await this.apiProvider.errAlert();
          alert.present();
          loading.dismiss();
        }
      );
    } else {
      this.isSubmited2 = true;
    }
  }
  async changeProfilePicture() {
    let imgAction: any
    this.translate.get("imgAction").subscribe((res: any) => {
      imgAction = res;
    });
    const actionSheet = await this.actionSheetCtrl.create({
      header: imgAction.source,
      buttons: [
        {
          text: imgAction.cam,
          icon: "camera",
          handler: () => {
            this.selectOptions(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: imgAction.gal,
          icon: "image",
          handler: () => {
            this.selectOptions(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        }
      ]
    });
    actionSheet.present();
  }
  async selectOptions(sourceType) {
    const loading = await this.loaderCtrl.create();
    loading.present();
    let imgAction: any
    this.translate.get("imgAction").subscribe((res: any) => {
      imgAction = res;
    });
    const options: CameraOptions = {
      quality: 70,
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType,
      targetWidth: 350,
      targetHeight: 350
    };
    this.camera.getPicture(options).then(
      imageData => {
        this.base64Image["profileImage"] = imageData;
        this.apiProvider.changeUsrProfilePicture(this.base64Image).subscribe(async res => {
          if (res['status'] == 1) {
            if (localStorage.getItem("circleUser")) {
              let oldData = JSON.parse(localStorage.getItem("circleUser"));
              oldData.profileImage = res['profile_image'];
              this.apiProvider.currentUser.proPic = res['profile_image'];
              this.profilePic = this.apiProvider.baseUrl + res['profile_image'];
              localStorage.setItem("circleUser", JSON.stringify(oldData));
              this.events.publish('user:proUpdate', JSON.stringify({ proImg: res['profile_image'] }));
            }
            const toastLogin = await this.toastCtrl.create({
              message: imgAction.proImgChanged,
              duration: 3000,
              buttons: [imgAction.ok]
            });
            toastLogin.present();
          } else {
            const toastLogin = await this.toastCtrl.create({
              message: res["msg"],
              duration: 3000,
              buttons: [imgAction.ok]
            });
            toastLogin.present();
          }
          loading.dismiss();
        }, async err => {
          const alert = await this.apiProvider.errAlert();
          alert.present();
          loading.dismiss();
        });
      },
      async err => {
        const toastLogin = await this.toastCtrl.create({
          message: imgAction.proImgErr,
          duration: 3000,
          buttons: [imgAction.ok]
        });
        loading.dismiss();
        toastLogin.present();
        console.log("Camera issue: " + err);
      }
    );
  }
  //---------------------------------------
  myImage() {
    this.profilePic = "./assets/imgs/pro_img.png";
  }
  //---------------------------------------

}
