import { ApiProviderService } from "./../../service/api-provider.service";
import { Component, OnInit } from "@angular/core";
import {
  ToastController,
  NavController,
  MenuController,
  LoadingController,
  Events
} from "@ionic/angular";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;
  public isSubmited: boolean = false;
  public language: any;
  constructor(
    private apiProvider: ApiProviderService,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    private menuCtrl: MenuController,
    private formBuilder: FormBuilder,
    private events: Events
  ) {
  }
  ionViewWillEnter() {
    if (this.apiProvider.isLoggedIn()) {
      
      this.navCtrl.navigateRoot("/tabs");
    }
    this.menuCtrl.enable(false);
    this.language = this.apiProvider.language;
  }
  public dataUser = { email: "", password: "" };

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: new FormControl('', Validators.compose([Validators.required])),
      password: new FormControl('', Validators.compose([Validators.required]))
    })
  }
  async loginUser() {
    if (this.loginForm.valid) {
      if (this.dataUser.email == 'user@user.com' && this.dataUser.password == '123456') {
        this.navCtrl.navigateRoot("/tabs");
      }
      // this.apiProvider.loginUser(this.dataUser);
    } else {
      this.isSubmited = true;
    }
  }
  //---------------------------------------------------------------------
  changeLanguage() {
    this.apiProvider.language = this.language;
    this.events.publish("user:changeLang", this.language);
  }
  //---------------------------------------------------------------
  public validation_messages = {
    'username': [
      { type: 'required', message: 'vMsg.uNameReq' }
    ],
    'password': [
      { type: 'required', message: 'vMsg.passReq' }
    ]
  };
}
