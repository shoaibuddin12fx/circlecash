import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { ApiProviderService } from 'src/app/service/api-provider.service';

@Component({
  selector: 'app-change',
  templateUrl: './change.page.html',
  styleUrls: ['./change.page.scss'],
})
export class ChangePage implements OnInit {
  public userPasswordData: any = {
    oldPassword: "",
    newPassword: "",
    userId: this.apiProvider.currentUser.id
  };
  public isSubmited: boolean = false;
  public isSubmited2: boolean = false;
  public changePassForm: FormGroup;
  constructor(
    public apiProvider: ApiProviderService,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private loaderCtrl: LoadingController,
    public formBuilder: FormBuilder,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.changePassForm = this.formBuilder.group({
      oldPass: new FormControl('', Validators.compose([Validators.required])),
      newPass: new FormControl('', Validators.compose([Validators.required]))
    });
  }

  async changeUserPasswordProfile() {
    if (this.changePassForm.valid) {
      const loading = await this.loaderCtrl.create();
      loading.present();
      let successMsg: any
      this.translate.get("resMsg").subscribe((res: any) => {
        successMsg = res;
      });
      this.apiProvider.changeUserPasswordProfile(this.userPasswordData).subscribe(
        async res => {
          loading.dismiss();
          if (res['status'] == 1) {
            const toastLogin = await this.toastCtrl.create({
              message: successMsg.passChanged,
              duration: 3000,
              buttons: [successMsg.ok]
            });
            toastLogin.present();
            toastLogin.onWillDismiss().then(res => this.navCtrl.navigateRoot("/tabs"));
          } else {
            const toastLogin = await this.toastCtrl.create({
              message: res["msg"],
              duration: 3000,
              buttons: [successMsg.ok]
            });
            toastLogin.present();
          }
        },
        async err => {
          const alert = await this.apiProvider.errAlert();
          alert.present();
          loading.dismiss();
        }
      );
    } else {
      this.isSubmited2 = true;
    }
  }

  //---------------------------------------------------------------
  public validation_messages = {
    'name': [
      { type: 'required', message: 'vMsg.nameReq' }
    ],
    'uName': [
      { type: 'required', message: 'vMsg.uNameReq' },
    ],
    'email': [
      { type: 'required', message: 'vMsg.emailReq' },
      { type: 'pattern', message: 'vMsg.validEmail' }
    ],
    'mobile': [
      { type: 'required', message: 'vMsg.mobileReq' }
    ],
    'address': [
      { type: 'required', message: 'vMsg.addReq' }
    ],
    'country': [
      { type: 'required', message: 'vMsg.countryReq' }
    ],
    'state': [
      { type: 'required', message: 'vMsg.stateReq' }
    ],
    'city': [
      { type: 'required', message: 'vMsg.cityReq' }
    ],
    'zipCode': [
      { type: 'required', message: 'vMsg.zipReq' }
    ],
    'oldPass': [
      { type: 'required', message: 'vMsg.oldPassReq' }
    ],
    'newPass': [
      { type: 'required', message: 'vMsg.newPassReq' }
    ]
  };
}
