import { Component, OnInit } from '@angular/core';
import { ApiProviderService } from 'src/app/service/api-provider.service';
import { Events, NavController } from '@ionic/angular';

@Component({
  selector: 'app-language',
  templateUrl: './language.page.html',
  styleUrls: ['./language.page.scss'],
})
export class LanguagePage implements OnInit {
  public language: any;
  constructor(
    public apiProvider: ApiProviderService,
    public events: Events,
    private navCtrl: NavController
  ) { }
  ngOnInit() {
  }
  async ionViewWillEnter() {
    this.language = this.apiProvider.language;
    if (this.apiProvider.isLoggedIn()) {
      this.navCtrl.navigateRoot("/tabs");
    }
  }
  changeLanguage() {
    this.events.publish("user:changeLang", this.language);
    if (this.apiProvider.isLoggedIn()) {
      this.navCtrl.navigateRoot("/tabs");
    } else {
      this.navCtrl.navigateRoot("/login");
    }
  }
}
