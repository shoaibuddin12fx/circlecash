import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiProviderService } from 'src/app/service/api-provider.service';
import { ToastController, NavController, LoadingController } from '@ionic/angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-create-password',
  templateUrl: './create-password.page.html',
  styleUrls: ['./create-password.page.scss'],
})
export class CreatePasswordPage implements OnInit {
  public resetPassForm: FormGroup;
  public isSubmited: boolean = false;
  constructor(
    private activatedRoot: ActivatedRoute,
    private apiProvider: ApiProviderService,
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private navCtrl: NavController,
    private loaderCtrl: LoadingController,
    private translate: TranslateService
  ) { }
  public newData: any = {
    authToken: "",
    userName: "",
    activationCode: "",
    password: "",
    cpassword: ""
  };
  ngOnInit() {
    this.resetPassForm = this.formBuilder.group({
      otp: new FormControl('', Validators.compose([Validators.required])),
      password: new FormControl('', Validators.compose([Validators.required])),
      cpassword: new FormControl('', Validators.compose([Validators.required]))
    }, {
      validators: this.password.bind(this)
    })
  }
  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: cpassword } = formGroup.get('cpassword');
    return password === cpassword ? null : { passwordNotMatch: true };
  }
  ionViewWillEnter() {
    this.newData.authToken = this.activatedRoot.snapshot.paramMap.get("authToken");
    this.newData.userName = this.activatedRoot.snapshot.paramMap.get("userName");
  }
  async setUserPassword() {
    if (this.resetPassForm.valid) {
      const loading = await this.loaderCtrl.create();
      loading.present();
      let successMsg: any;
      this.translate.get("resMsg").subscribe((res: any) => {
        successMsg = res;
      });
      this.apiProvider.setUserPassword(this.newData).subscribe(async res => {
        loading.dismiss();
        if (res["status"] == 1) {
          const toastForgotPassword = await this.toastCtrl.create({
            message: successMsg.pResetDone,
            duration: 3000,
            buttons: [successMsg.ok]
          });
          toastForgotPassword.present();
          this.navCtrl.navigateForward("login");
        } else {
          const toastForgotPassword = await this.toastCtrl.create({
            message: res["msg"],
            duration: 3000,
            buttons: [successMsg.ok]
          });
          toastForgotPassword.present();
        }
      }, async err => {
        loading.dismiss();
        const alert = await this.apiProvider.errAlert();
        alert.present();
      });
    } else {
      this.isSubmited = true;
    }
  }
  //---------------------------------------------------------------
  public validation_messages = {
    'otp': [
      { type: 'required', message: 'vMsg.otpReq' }
    ],
    'password': [
      { type: 'required', message: 'vMsg.passReq' }
    ],
    'cpassword': [
      { type: 'required', message: 'vMsg.cPassReq' }
    ]
  };
}
