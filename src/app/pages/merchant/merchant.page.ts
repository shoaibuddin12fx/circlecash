import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-merchant',
  templateUrl: './merchant.page.html',
  styleUrls: ['./merchant.page.scss'],
})
export class MerchantPage implements OnInit {
  public merchantID: any;
  public payMerForm: FormGroup;
  public payAmount: any;
  public isSubmited: boolean = false;
  constructor(
    private activatedRoot: ActivatedRoute,
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
  ) {
    this.merchantID = this.activatedRoot.snapshot.paramMap.get("merchantID");
  }

  ngOnInit() {
    this.payMerForm = this.formBuilder.group({
      payAmount: new FormControl('', Validators.compose([Validators.required]))
    })
  }
  async payMerchant() {
    if (this.payMerForm.valid) {
      this.navCtrl.navigateForward(`/payment/${this.merchantID}/${this.payAmount}`);
    } else {
      this.isSubmited = true;
    }
  }
  public validation_messages = {
    'payAmount': [
      { type: 'required', message: 'vMsg.moneyReq' }
    ]
  }
}
