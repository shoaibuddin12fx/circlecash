import {
  ToastController,
  NavController,
  MenuController,
  LoadingController,
  AlertController
} from "@ionic/angular";
import { ApiProviderService } from "./../../service/api-provider.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: "app-registration",
  templateUrl: "./registration.page.html",
  styleUrls: ["./registration.page.scss"]
})
export class RegistrationPage implements OnInit {
  public dataUser = {
    name: "",
    email: "",
    password: "",
    cpassword: "",
    mobile: "",
    userName: ""
  };
  public isSubmited: boolean = false;
  public registrationForm: FormGroup;
  constructor(
    public formBuilder: FormBuilder,
    private apiProvider: ApiProviderService,
    private toastCtrl: ToastController,
    private navCtrl: NavController,
    private loaderCtrl: LoadingController,
    private translate: TranslateService
  ) { }
  ngOnInit() {
    const EMAILPATTERN = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-z\-0-9]+\.)+[a-z]{2,}))$/;
    this.registrationForm = this.formBuilder.group({
      name: new FormControl('', Validators.compose([Validators.required])),
      uName: new FormControl('', Validators.compose([Validators.required])),
      mobile: new FormControl('', Validators.compose([Validators.required])),
      email: new FormControl('', Validators.compose([Validators.pattern(EMAILPATTERN), Validators.required])),
      password: new FormControl('', Validators.compose([Validators.required])),
      cpassword: new FormControl('', Validators.compose([Validators.required])),
    }, {
      validators: this.password.bind(this)
    })
  }
  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: cpassword } = formGroup.get('cpassword');
    return password === cpassword ? null : { passwordNotMatch: true };
  }
  async addUser() {
    if (this.registrationForm.valid) {
      const loading = await this.loaderCtrl.create();
      loading.present();
      let successMsg: any
      this.translate.get("resMsg").subscribe((res: any) => {
        successMsg = res;
      });
      this.apiProvider.addUser(this.dataUser).subscribe(
        async res => {
          if (res["status"] == 1) {
            loading.dismiss();
            const toastLogin = await this.toastCtrl.create({
              message: successMsg.regDone,
              duration: 3000,
              buttons: [successMsg.ok]
            });
            toastLogin.present();
            this.navCtrl.navigateForward("/login");
          } else {
            const toastLogin = await this.toastCtrl.create({
              message: res["msg"],
              duration: 3000,
              buttons: [successMsg.ok]
            });
            toastLogin.present();
          }
        },
        async err => {
          const alert = await this.apiProvider.errAlert();
          loading.dismiss();
          alert.present();
        }
      );
    } else {
      this.isSubmited = true;
    }
  }
  public validation_messages = {
    'name': [
      { type: 'required', message: 'vMsg.nameReq' }
    ],
    'uName': [
      { type: 'required', message: 'vMsg.uNameReq' },
    ],
    'email': [
      { type: 'required', message: 'vMsg.emailReq' },
      { type: 'pattern', message: 'vMsg.validEmail' }
    ],
    'mobile': [
      { type: 'required', message: 'vMsg.mobileReq' }
    ],
    'password': [
      { type: 'required', message: 'vMsg.passReq' }
    ],
    'cpassword': [
      { type: 'required', message: 'vMsg.cPassReq' }
    ]
  };
}
