import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { TabsPage } from "./tabs.page";
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: "tabs",
    component: TabsPage,
    children: [
      {
        path: "tab1",
        loadChildren: "../preview/preview.module#PreviewPageModule"
      },
      { path: "tab2", loadChildren: "../pay/pay.module#PayPageModule" },
      {
        path: "tab3",
        loadChildren:
          "../payment-history/payment-history.module#PaymentHistoryPageModule"
      }
    ]
  },
  {
    path: "",
    redirectTo: "tabs/tab1",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule { }
