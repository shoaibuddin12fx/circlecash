import { ApiProviderService } from "./../../service/api-provider.service";
import { Component, OnInit } from "@angular/core";
import { LoadingController } from '@ionic/angular';

@Component({
  selector: "app-payment-history",
  templateUrl: "./payment-history.page.html",
  styleUrls: ["./payment-history.page.scss"]
})
export class PaymentHistoryPage implements OnInit {
  constructor(
    private apiProvider: ApiProviderService,
    private loadingCtrl: LoadingController,
  ) { }
  private userData = { userId: this.apiProvider.currentUser.id };
  public paymentHistory: any = [];
  async  ionViewWillEnter() {
    const loader = await this.loadingCtrl.create();
    loader.present();
    this.apiProvider.getPaymentHistory(this.userData).subscribe(res => {
      loader.dismiss();
      if (res['status'] == 1) {
        this.paymentHistory = res["data"];
      } else {
        this.paymentHistory = [];
      }
    }, async err => {
      loader.dismiss();
      const alert = await this.apiProvider.errAlert();
      alert.present();
    });
  }

  ngOnInit() { }
}
