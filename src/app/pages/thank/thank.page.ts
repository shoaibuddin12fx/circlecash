import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-thank',
  templateUrl: './thank.page.html',
  styleUrls: ['./thank.page.scss'],
})
export class ThankPage implements OnInit {

  constructor(
    public navCtrl: NavController
  ) { }

  ngOnInit() {
  }
  payOther() {
    this.navCtrl.navigateForward(`/tabs/tabs/tab2`);
  }
}
