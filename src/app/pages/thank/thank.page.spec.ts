import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThankPage } from './thank.page';

describe('ThankPage', () => {
  let component: ThankPage;
  let fixture: ComponentFixture<ThankPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThankPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThankPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
