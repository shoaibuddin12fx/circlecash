import { ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { AlertController, LoadingController, NavController } from "@ionic/angular";
import { ApiProviderService } from 'src/app/service/api-provider.service';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: "app-payment",
  templateUrl: "./payment.page.html",
  styleUrls: ["./payment.page.scss"]
})
export class PaymentPage implements OnInit {
  public merchantID: any;
  public payAmount: any;
  public isSubmited: boolean = false;
  public payForm: FormGroup;
  constructor(
    public formBuilder: FormBuilder,
    private activatedRoot: ActivatedRoute,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    private apiProvider: ApiProviderService,
    private translate: TranslateService,
  ) {
    this.merchantID = this.activatedRoot.snapshot.paramMap.get("merchantID");
    this.payAmount = this.activatedRoot.snapshot.paramMap.get("payAmount");
  }
  async ionViewWillEnter() {
    // let payAction: any;
    // this.translate.get("others").subscribe((res: any) => {
    //   payAction = res;
    // });
    // const alert = await this.alertCtrl.create({
    //   header: payAction.payAlert,
    //   message: `${payAction.merchantID} : ${this.merchantID}, <br /> ${payAction.payAmount} : $ ${this.payAmount}`,
    //   buttons: [payAction.ok]
    // });
    // alert.present();
  }
  ngOnInit() {
    this.payForm = this.formBuilder.group({
      cardno: new FormControl('', Validators.compose([Validators.required])),
      expiry: new FormControl('', Validators.compose([Validators.required])),
      cvv: new FormControl('', Validators.compose([Validators.required])),
    })
  }
  public validation_messages = {
    'cardno': [
      { type: 'required', message: 'vMsg.cardnoReq' }
    ],
    'expiry': [
      { type: 'required', message: 'vMsg.expiryReq' },
    ],
    'cvv': [
      { type: 'required', message: 'vMsg.cvvReq' },
    ],
  }
  async  dummyPayment() {
    if (this.payForm.valid) {
      const loader = await this.loadingCtrl.create();
      loader.present();
      let userData = { userId: this.apiProvider.currentUser.id, amount: this.payAmount };
      this.apiProvider.dummyPayment(userData).subscribe(res => {
        loader.dismiss();
        if (res['status'] == 1) {
          this.navCtrl.navigateForward(`/thank`);
        } else {
        }
      }, async err => {
        loader.dismiss();
        const alert = await this.apiProvider.errAlert();
        alert.present();
      });
    } else {
      this.isSubmited = true;
    }
  }
}
