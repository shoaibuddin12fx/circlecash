import {
  ToastController,
  NavController,
  LoadingController,
  AlertController
} from "@ionic/angular";
import { ApiProviderService } from "./../../service/api-provider.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: "app-forgot",
  templateUrl: "./forgot.page.html",
  styleUrls: ["./forgot.page.scss"]
})
export class ForgotPage implements OnInit {
  public userData = {
    userName: "",
    authToken: ""
  };
  public forGotPassForm: FormGroup;
  public isSubmited: boolean = false;
  constructor(
    private apiProvider: ApiProviderService,
    private toastCtrl: ToastController,
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
    private loaderCtrl: LoadingController,
    private translate: TranslateService
  ) { }
  ngOnInit() {
    this.forGotPassForm = this.formBuilder.group({
      username: new FormControl('', Validators.compose([Validators.required]))
    })
  }
  async forgotPassword() {
    if (this.forGotPassForm.valid) {
      const loading = await this.loaderCtrl.create();
      loading.present();
      let successMsg: any;
      this.translate.get("resMsg").subscribe((res: any) => {
        successMsg = res;
      });
      this.apiProvider.forgotPasswordCustomer(this.userData).subscribe(
        async res => {
          if (res['status'] == 1) {
            const toastForgotPassword = await this.toastCtrl.create({
              message: successMsg.otpSend,
              duration: 3000,
              buttons: [successMsg.ok]
            });
            loading.dismiss();
            toastForgotPassword.present();
            let authToken = res["data"]["authToken"];
            let userName = res["data"]["userName"];
            this.navCtrl.navigateForward(`/create-password/${authToken}/${userName}`);
          } else {
            const toastForgotPassword = await this.toastCtrl.create({
              message: res["msg"],
              duration: 3000,
              buttons: [successMsg.ok]
            });
            loading.dismiss();
            toastForgotPassword.present();
          }
        },
        async err => {
          loading.dismiss();
          const alert = await this.apiProvider.errAlert();
          alert.present();
        }
      );
    } else {
      this.isSubmited = true;
    }
  }
  public validation_messages = {
    'username': [
      { type: 'required', message: 'vMsg.uNameReq' }
    ]
  };
}
