import { NavController } from "@ionic/angular";
import {
  BarcodeScanner,
  BarcodeScannerOptions
} from "@ionic-native/barcode-scanner/ngx";
import { Component, OnInit } from "@angular/core";
@Component({
  selector: "app-pay",
  templateUrl: "./pay.page.html",
  styleUrls: ["./pay.page.scss"]
})
export class PayPage implements OnInit {
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;
  constructor(
    private barCodeScanner: BarcodeScanner,
    private navCtrl: NavController
  ) { }
  public qrCode: any;
  scanAndpay() {
    this.barCodeScanner.scan({
      resultDisplayDuration: 0,
      showTorchButton: true,
      formats: "QR_CODE"
    }).then(barcodeData => {
      // console.log(barcodeData);
      if (barcodeData.text.length > 0) {
        this.scannedData = barcodeData;
        this.navCtrl.navigateForward(`/merchant/${this.scannedData["text"]}`);
      }
    }).catch(err => {
      console.log("Error", err);
    });
  }
  ngOnInit() { }
}
