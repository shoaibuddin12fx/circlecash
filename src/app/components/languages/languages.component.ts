import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.scss'],
})
export class LanguagesComponent implements OnInit {

  @Input() title: string;
  @Input() activLang: string;
  @Input() languages: Array<{ name: string, value: string }>;
  @Output() change: EventEmitter<string> = new EventEmitter<string>();
  public isLangOpen: boolean = false;

  constructor() { }

  ngOnInit() { }

  public toggleLang() {
    this.isLangOpen = !this.isLangOpen;
  }

  public broadcastLang(langValue: string): void {
    this.change.emit(langValue);
    this.isLangOpen = false;
  }
}
