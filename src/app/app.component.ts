import { ApiProviderService } from "./service/api-provider.service";
import { Component, Inject, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { Platform, NavController, MenuController, Events, IonRouterOutlet, ToastController } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { DOCUMENT } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
})
export class AppComponent {
  // hardware back button event.
  @ViewChild(IonRouterOutlet, { static: true }) routerOutlet: IonRouterOutlet;
  public menuSide: string;
  public appPages = [
    { title: "hf.home", url: "/tabs", icon: "list" },
    { title: "hf.pay", url: "/tabs/tabs/tab2", icon: "list" },
    { title: "hf.payHistory", url: "/tabs/tabs/tab3", icon: "list" },
    // { title: "hf.language", url: "/language", icon: "list" },
    // { title: "others.thankYou", url: "/thank", icon: "list" },
    // { title: "others.merchant", url: "/merchant/1", icon: "list" },
    // { title: "hf.changePass", url: "/change-password", icon: "list" },
    // { title: "pay.payment", url: "/payment/1/10", icon: "list" },
  ];
  constructor(
    @Inject(DOCUMENT) private doc,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public apiService: ApiProviderService,
    private navCtrl: NavController,
    private menuCtrl: MenuController,
    private tostCtrl: ToastController,
    private events: Events,
    private translate: TranslateService,
  ) {
    this.initializeApp();
    this.initTranslate();
    this.events.subscribe("user:changeLang", res => {
      this.changeLanguage(res);
    });
    this.events.subscribe('user:proUpdate', res => {
      let data = JSON.parse(res);
      this.profilePic = this.apiService.baseUrl + data.proImg;
    });
  }
  public profilePic: any;
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString("#033c15");
      this.splashScreen.hide();
      this.menuCtrl.enable(false);
      this.backButtonEvent();
      if (this.apiService.isLoggedIn()) {
        this.menuCtrl.enable(true);
        this.appPages.push({ title: "hf.changePass", url: "/change-password", icon: "list" });
        this.profilePic = this.apiService.baseUrl + this.apiService.currentUser.proPic;
        let lang = localStorage.getItem("circleAppLang");
        if (lang != undefined || lang != null) {
          this.changeLanguage(lang, false);
        }
        this.navCtrl.navigateRoot("/tabs");
      } else {
        this.profilePic = "./assets/imgs/menu-profile.png";
      }
    });
  }
  //---------------------------------------
  myImage() {
    this.profilePic = "./assets/imgs/menu-profile.png";
  }
  //---------------------------------------
  logout() {
    this.apiService.logoutUser();
    this.navCtrl.navigateRoot("/login");
  }
  //----------------------------------------------------
  changeLanguage(lang, isSet = true) {
    if (isSet)
      localStorage.setItem("circleAppLang", lang);
    this.translate.use(lang);
    this.apiService.language = lang;
    if (lang === "ar") {
      this.menuSide = "end";
      this.doc.dir = "rtl";
      this.translate.use(lang);
    } else {
      this.menuSide = "start";
      this.doc.dir = "ltr";
      this.translate.use(lang);
    }
  }
  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang("en");
    if (this.translate.getBrowserLang() !== undefined) {
      this.apiService.language = this.translate.getBrowserLang();
      if (this.translate.getBrowserLang() == "ar") {
        document.documentElement.dir = "rtl";
        this.menuSide = "end";
      }
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.apiService.language = "ar";
      document.documentElement.dir = "rtl";
      this.menuSide = "end";
      this.translate.use("ar"); // Set your language here
    }
  }
  //----------------------------------------------------
  //---------------------------------------
  // Active hardware back button
  //---------------------------------------
  backButtonEvent() {
    let lastTimeBackPress = 0;
    let timePeriodToExit = 2000;
    this.platform.backButton.subscribeWithPriority(999999, async () => {
      if (await this.menuCtrl.isOpen()) {
        this.menuCtrl.close();
      }
      //this.platform.backButton.subscribe(async () => {
      //this.router.url === '/LoginPage'
      if (this.routerOutlet && this.routerOutlet.canGoBack()) {
        this.routerOutlet.pop();
      } else if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
        navigator["app"].exitApp();
      } else {
        let backMsg: any;
        await this.translate.get("backMsg").subscribe((res: any) => {
          backMsg = res;
        });
        const toast = await this.tostCtrl.create({
          message: backMsg,
          color: "dark",
          duration: 2000
        });
        await toast.present();
        lastTimeBackPress = new Date().getTime();
      }
    });
  }
  //----------------------------------------------------
  public languages: Array<{ name: string, value: string }> = [
    {
      name: 'lang.english',
      value: 'en'
    },
    {
      name: 'lang.french',
      value: 'fr'
    },
    {
      name: 'lang.arabic',
      value: 'ar'
    }
  ];
}